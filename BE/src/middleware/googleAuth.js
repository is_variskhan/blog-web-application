const jwt = require("jsonwebtoken");
const googleUser = require("../models/googleUser");

const auth = async (req, res, next) => {
  try {
    const token = req.header("Authorization").replace("Bearer ", "");
    const decode = jwt.verify(token, process.env.JWT_SECRET);
    const user = await googleUser.findOne({
      _id: decode._id,
      "access_token.token": token,
    });

    if (!user) {
      throw new Error();
    }

    req.token = token;
    req.googleUser = user;
    next();
  } catch (error) {
    res.status(401).send({ error: "Please Authrization yourself!" });
  }
};

module.exports = auth;
