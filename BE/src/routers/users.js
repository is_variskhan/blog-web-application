const { Router } = require("express");
const multer = require("multer");
const sharp = require("sharp");
const passport = require("passport");

const User = require("../models/users");
const googleUser = require("../models/googleUser");
const auth = require("../middleware/auth");
const googleAuth = require("../middleware/googleAuth");
const handleUserError = require("../validate/handleUserErrors");
const { sendWelcomeMail, sendCancelationMail } = require("../emails/account");

const router = Router();

router.post("/singup", async (req, res) => {
  const user = new User(req.body);
  try {
    await user.save();
    sendWelcomeMail(user.email, user.name);
    const token = await user.createToken();
    res.status(201).send({ user, token });
  } catch (err) {
    const errors = handleUserError(err);
    res.status(400).send({ errors });
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findByCredential(email, password);
    const token = await user.createToken();
    res.status(200).send({ user, token });
  } catch (err) {
    const errors = handleUserError(err);
    res.status(400).send({ errors });
  }
});

router.get("/users/me", auth, async (req, res) => {
  res.send(req.user);
});

router.post("/users/logout", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();

    res.send();
  } catch (err) {
    const errors = handleUserError(err);
    res.status(500).send({ errors });
  }
});

router.post("/users/logoutAll", auth, async (req, res) => {
  try {
    req.user.tokens = [];
    await req.user.save();

    res.send();
  } catch (err) {
    const errors = handleUserError(err);
    res.status(500).send({ errors });
  }
});

router.patch("/users/me", auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedToUpdate = ["name", "email", "password", "age"];
  const isValidProprtey = updates.every((update) =>
    allowedToUpdate.includes(update)
  );

  if (!isValidProprtey) {
    return res.status(400).send({ error: "Invalid Propery to update!" });
  }

  try {
    updates.forEach((update) => (req.user[update] = req.body[update]));
    await req.user.save();

    res.status(200).send(req.user);
  } catch (error) {
    res.status(500).send();
  }
});

router.delete("/users/me", auth, async (req, res) => {
  try {
    await req.user.remove();
    sendCancelationMail(req.user.email, req.user.name);
    res.send(req.user);
  } catch (err) {
    const errors = handleUserError(err);
    res.status(500).send({ errors });
  }
});

//filter image size and image type
const upload = multer({
  limits: {
    fieldSize: 1000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error("Please upload JPG File."));
    }
    cb(undefined, true);
  },
});

//Upload profile pic
router.post(
  "/users/me/avatar",
  auth,
  upload.single("avatar"),
  async (req, res) => {
    const buffer = await sharp(req.file.buffer)
      .resize({ width: 250, height: 250 })
      .png()
      .toBuffer();

    req.user.avatar = buffer;

    await req.user.save();
    res.send(req.user);
  },
  (error, req, res, next) => {
    res.status(400).send({ error: error.message });
  }
);

//get Profile pic
router.get("/users/:id/avatar", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    if (!user || !user.avatar) {
      throw new Error();
    }

    res.set("Content-Type", "image/png");
    res.send(user.avatar);
  } catch (error) {
    res.status(404).send();
  }
});

//Delete Profile pic
router.delete("/users/me/avatar", auth, async (req, res) => {
  req.user.avatar = undefined;
  await req.user.save();
  res.send();
});

module.exports = router;
