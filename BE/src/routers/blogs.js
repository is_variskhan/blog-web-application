const express = require("express");

const Blog = require("../models/blogs");
const googleUserBlog = require("../models/googleUserBlog");
const auth = require("../middleware/auth");
const roleBaseAuth = require("../middleware/roleBaseAuth");
const handleBlogErrors = require("../validate/handleBlogErrors");

const router = new express.Router();

router.post("/create-blog", auth, async (req, res) => {
  const blog = new Blog({
    ...req.body,
    user_id: req.user._id,
  });
  try {
    await blog.save();
    res.status(201).send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(400).send({ errors });
  }
});

router.get("/blogs/:id", auth, roleBaseAuth("user"), async (req, res) => {
  const _id = req.params.id;
  try {
    const blog = await Blog.findOne({ _id, user_id: req.user._id });
    if (!blog) {
      return res.status(400).send();
    }
    res.send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

//read all blogs by user loggned in
//read sortby url params to ascn and desc
//url:- http://localhost:3000/blogs?sortBy=createdAt:desc

router.get("/all-blogs", auth, roleBaseAuth("admin"), async (req, res) => {
  const sort = {};

  if (req.query.sortBy) {
    const parts = req.query.sortBy.split(":");
    sort[parts[0]] = parts[1] === "desc" ? -1 : 1;
  }

  try {
    const blogs = await Blog.find();
    const googleBlog = await googleUserBlog.find()
    const allblogs = [...blogs, ...googleBlog]
    res.send(allblogs);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

router.get("/all-blogs/:id", auth, roleBaseAuth("admin"), async (req, res) => {
  const _id = req.params.id;
  try {
    const blog = await Blog.findOne({ _id });
    if (!blog) {
      return res.status(400).send();
    }
    res.send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

router.get("/blogs", auth, async (req, res) => {
  const sort = {};

  if (req.query.sortBy) {
    const parts = req.query.sortBy.split(":");
    sort[parts[0]] = parts[1] === "desc" ? -1 : 1;
  }

  try {
    await req.user
      .populate({
        path: "myBlog",
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.skip),
          sort,
        },
      })
      .execPopulate();
    res.send(req.user.myBlog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

router.patch("/blogs/:id", auth, async (req, res) => {
  const updated = Object.keys(req.body);
  const allowToUpdate = ["title", "body"];
  const isValidProperty = updated.every((item) => {
    return allowToUpdate.includes(item);
  });

  if (!isValidProperty) {
    return res.status(400).send({ error: "Invalid Propery to update!" });
  }
  try {
    const blog = await Blog.findOne({
      _id: req.params.id,
      user_id: req.user._id,
    });

    if (!blog) {
      return res.status(400).send();
    }
    updated.forEach((item) => (blog[item] = req.body[item]));

    await blog.save();
    res.status(200).send(blog);
  } catch (error) {
    const errors = handleBlogErrors(error);
    res.status(500).send({ errors });
  }
});

router.delete("/blogs/:id", auth, async (req, res) => {
  const _id = req.params.id;
  try {
    const blog = await Blog.findOneAndDelete({ _id, user_id: req.user._id });

    if (!blog) {
      return res.status(400).send();
    }
    res.send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

module.exports = router;
