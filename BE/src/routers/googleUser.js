const { Router } = require("express");
const passport = require("passport");

const googleAuth = require("../middleware/googleAuth");
const googleUserBlog = require("../models/googleUserBlog");
const handleUserError = require("../validate/handleUserErrors");
const handleBlogErrors = require("../validate/handleBlogErrors");
const { sendWelcomeMail } = require("../emails/account");

const router = Router();

//sing in with google
router.get(
  "/google-login",
  passport.authenticate("google", {
    scope: ["profile", "email"],
  })
);

//callback route for google to redirect to
router.get(
  "/google-login/redirect",
  passport.authenticate("google", { session: false }),
  async (req, res) => {
    const user = req.user;
    const token = await user.createGoogleUserToken();
    sendWelcomeMail(user.email, user.name);
    res.redirect("http://localhost:3001?token=" + token);
  }
);

router.get("/google-user/me", googleAuth, async (req, res) => {
  res.send(req.googleUser);
});

router.post("/google-user/logout", googleAuth, async (req, res) => {
  try {
    req.googleUser.access_token = req.googleUser.access_token.filter(
      (token) => {
        return token.token !== req.token;
      }
    );
    await req.googleUser.save();

    res.send();
  } catch (err) {
    const errors = handleUserError(err);
    res.status(500).send({ errors });
  }
});

router.post("/google-user/create-blog", googleAuth, async (req, res) => {
  const blog = new googleUserBlog({
    ...req.body,
    googleUser_id: req.googleUser._id,
  });
  try {
    await blog.save();
    res.status(201).send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(400).send({ errors });
  }
});

router.get("/google-user/blogs", googleAuth, async (req, res) => {
  const sort = {};

  if (req.query.sortBy) {
    const parts = req.query.sortBy.split(":");
    sort[parts[0]] = parts[1] === "desc" ? -1 : 1;
  }

  try {
    await req.googleUser
      .populate({
        path: "googleBlog",
        options: {
          limit: parseInt(req.query.limit),
          skip: parseInt(req.query.skip),
          sort,
        },
      })
      .execPopulate();
    res.send(req.googleUser.googleBlog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

router.get("/google-user/blogs/:id", googleAuth, async (req, res) => {
  const _id = req.params.id;
  try {
    const blog = await googleUserBlog.findOne({
      _id,
      googleUser_id: req.googleUser._id,
    });
    if (!blog) {
      return res.status(400).send();
    }
    res.send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

router.patch("/google-user/blogs/:id", googleAuth, async (req, res) => {
  const updated = Object.keys(req.body);
  const allowToUpdate = ["title", "body"];
  const isValidProperty = updated.every((item) => {
    return allowToUpdate.includes(item);
  });

  if (!isValidProperty) {
    return res.status(400).send({ error: "Invalid Propery to update!" });
  }
  try {
    const blog = await googleUserBlog.findOne({
      _id: req.params.id,
      googleUser_id: req.googleUser._id,
    });

    if (!blog) {
      return res.status(400).send();
    }
    updated.forEach((item) => (blog[item] = req.body[item]));

    await blog.save();
    res.status(200).send(blog);
  } catch (error) {
    const errors = handleBlogErrors(error);
    res.status(500).send({ errors });
  }
});

router.delete("/google-user/blogs/:id", googleAuth, async (req, res) => {
  const _id = req.params.id;
  try {
    const blog = await googleUserBlog.findOneAndDelete({
      _id,
      googleUser_id: req.googleUser._id,
    });

    if (!blog) {
      return res.status(400).send();
    }
    res.send(blog);
  } catch (err) {
    const errors = handleBlogErrors(err);
    res.status(500).send({ errors });
  }
});

module.exports = router;
