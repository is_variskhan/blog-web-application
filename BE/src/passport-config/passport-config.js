const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20");

const keys = require("../../config/keys");
const googleUser = require("../models/googleUser");

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  googleUser.findById(id).then((user) => {
    done(null, user);
  });
});

passport.use(
  new GoogleStrategy(
    {
      callbackURL: "/google-login/redirect",
      clientID: keys.google.clientID,
      clientSecret: keys.google.clientSecret,
      proxy: true,
    },
    (accessToken, refreshToken, profile, done) => {
      googleUser.findOne({ googleId: profile.id }).then((currentUser) => {
        if (currentUser) {
          // already have this user
          done(null, currentUser);
        } else {
          // if not, create user in our db
          let newUser = new googleUser();
          (newUser.googleId = profile.id),
            (newUser.name = profile.displayName),
            (newUser.email = profile._json.email),
            newUser.save((err) => {
              if (err) {
                throw err;
              }
              return done(null, newUser);
            });
        }
      });
    }
  )
);
