const Blog = require("../models/blogs");

const handleUserErrors = (err) => {
  const errors = { title: "", body: "" };

  //check dynamic validation of form
  if (err.message.includes("Blog validation failed")) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }
  return errors;
};

module.exports = handleUserErrors;
