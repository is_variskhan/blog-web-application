const User = require("../models/users");

const handleUserErrors = (err) => {
  const errors = { name: "", email: "", password: "" };

  // incorrect email
  if (err.message === "Incorrect Email") {
    errors.email = "Please provide a valid email address.";
  }

  // incorrect password
  if (err.message === "Incorrect Password") {
    errors.password = "Password is Incorrect.";
  }

  //dublicate Email
  if (err.code === 11000) {
    errors.email = "This Email is already Registered, Please use different.";
    return errors;
  }

  //check dynamic validation of form
  if (err.message.includes("User validation failed")) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }
  return errors;
};

module.exports = handleUserErrors;
