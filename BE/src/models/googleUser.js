const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Schema = mongoose.Schema;

const googleUserSchema = new Schema({
  email: String,
  googleId: String,
  name: String,
  access_token: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],
});

googleUserSchema.virtual("googleBlog", {
  ref: "googleUserBlog",
  localField: "_id",
  foreignField: "googleUser_id",
});

//create token
googleUserSchema.methods.createGoogleUserToken = async function () {
  const maxAge = 3 * 24 * 60 * 60;
  const token = jwt.sign({ _id: this._id.toString() }, process.env.JWT_SECRET, {
    expiresIn: maxAge,
  });
  this.access_token = this.access_token.concat({ token });
  await this.save();
  return token;
};

const GoogleUser = mongoose.model("googleUser", googleUserSchema);

module.exports = GoogleUser;
