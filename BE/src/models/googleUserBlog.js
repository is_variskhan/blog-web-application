const mongoose = require("mongoose");

const googleUserblogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Title is Require!"],
      trim: true,
    },
    body: {
      type: String,
      required: [true, "Body is Require!"],
      trim: true,
    },
    googleUser_id: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "googleUser",
    },
  },
  {
    timestamps: true,
  }
);

const googleUserBlog = mongoose.model("googleUserBlog", googleUserblogSchema);

module.exports = googleUserBlog;
