const mongoose = require("mongoose");
const { isEmail } = require("validator");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Blog = require("./blogs");

let validateAge = function (age) {
  let re = /^[1-9]?[0-9]{1}$|^100$/;
  return re.test(age);
};

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please enter your name"],
      trim: true,
    },
    age: {
      type: Number,
      validate: [
        (val) => {
          validateAge(val);
        },
        "The age must be a number between 1 and 100",
      ],
    },
    role: {
      type: String,
      default: "user",
      enum: ["user", "admin"],
    },
    avatar: {
      type: Buffer,
    },
    email: {
      type: String,
      required: [true, "Please enter an Email"],
      unique: true,
      trim: true,
      lowercase: true,
      validate: [isEmail, "Please enter Valid Email"],
    },
    password: {
      type: String,
      required: [true, "Password is Required"],
      trim: true,
      minlength: [8, "Minimum password length is 8 characters"],
    },
    tokens: [
      {
        token: {
          type: String,
          required: true,
        },
      },
    ],
  },
  {
    timestamps: true,
  }
);

//vritual relationship enitity b/w task and user which not created in db
userSchema.virtual("myBlog", {
  ref: "Blog",
  localField: "_id",
  foreignField: "user_id",
});

//create token
userSchema.methods.createToken = async function () {
  const maxAge = 3 * 24 * 60 * 60;
  const token = jwt.sign({ _id: this._id.toString() }, process.env.JWT_SECRET, {
    expiresIn: maxAge,
  });
  this.tokens = this.tokens.concat({ token });
  await this.save();
  return token;
};

//return back filter User data with toJSON (toJSON automatillcy gives us feature to modified data in res)
userSchema.methods.toJSON = function () {
  const userObject = this.toObject();
  delete userObject.password;
  delete userObject.tokens;
  delete userObject.avatar;
  return userObject;
};

//login check token
userSchema.statics.findByCredential = async function (email, pass) {
  const user = await this.findOne({ email });
  if (user) {
    const auth = await bcrypt.compare(pass, user.password);
    if (auth) {
      return user;
    }
    throw Error("Incorrect Password");
  }
  throw Error("Incorrect Email");
};

// Hash the pain text password before saving
userSchema.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await bcrypt.hash(this.password, 8);
  }

  next();
});

//Delete user tasks when user is removed
userSchema.pre("remove", async function (next) {
  await Blog.deleteMany({ user_id: this._id });
  next();
});

const User = mongoose.model("User", userSchema);

module.exports = User;
