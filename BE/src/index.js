const express = require("express");
const cors = require("cors");
require("./db/mongoose");
const passport = require("passport");

const passportSetup = require("./passport-config/passport-config");
const userRouter = require("./routers/users");
const blogRouter = require("./routers/blogs");
const googleUserRouter = require('./routers/googleUser')

const app = express();
const port = process.env.PORT;

app.use(passport.initialize());
app.use(cors());
app.use(express.json());
app.use(userRouter);
app.use(blogRouter);
app.use(googleUserRouter);

app.listen(port, () => {
  console.log("server is up on port", +port);
});
