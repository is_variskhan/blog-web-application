import React from "react";
import "./FormInput.css";

function FormInput(props) {
  let inputElement = null;
  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          type={
            props.fieldName === "text"
              ? "text"
              : props.fieldName === "password"
              ? "password"
              : props.fieldName === "number"
              ? "number"
              : "text"
          }
          name="title"
          className={props.editable ? "updated-input-field" : "input-field"}
          value={props.value}
          autoComplete={props.autoComplate}
          onChange={props.editable ? props.updateChanged : props.changed}
          placeholder={
            props.elementName === "Name"
              ? "Name"
              : props.elementName === "Age"
              ? "Age"
              : props.elementName === "Email"
              ? "Email"
              : props.elementName === "Password"
              ? "Password"
              : props.elementName === "Title"
              ? "Blog Title"
              : props.elementName === "Body"
              ? "Blog Content.."
              : "Text"
          }
        />
      );
      break;
    case "textarea":
      inputElement = (
        <textarea
          type={
            props.fieldName === "text"
              ? "text"
              : props.fieldName === "password"
              ? "password"
              : props.fieldName === "number"
              ? "number"
              : props.elementName === "Title"
              ? "Blog Title"
              : props.elementName === "Body"
              ? "Blog Content.."
              : "text"
          }
          name="content"
          autoComplete={props.autoComplate}
          className={props.editable ? "updated-input-field" : "input-field"}
          value={props.value}
          onChange={props.editable ? props.updateChanged : props.changed}
          placeholder={
            props.elementName === "Name"
              ? "Name"
              : props.elementName === "Age"
              ? "Age"
              : props.elementName === "Email"
              ? "Email"
              : props.elementName === "Password"
              ? "Password"
              : props.elementName === "Title"
              ? "Blog Title"
              : props.elementName === "Body"
              ? "Blog Content.."
              : "Text"
          }
        ></textarea>
      );
      break;
    default:
      inputElement = inputElement = (
        <input
          type={
            props.fieldName === "text"
              ? "text"
              : props.fieldName === "password"
              ? "password"
              : props.fieldName === "number"
              ? "number"
              : props.elementName === "Title"
              ? "Blog Title"
              : props.elementName === "Body"
              ? "Blog Content.."
              : "text"
          }
          name="title"
          className={props.editable ? "updated-input-field" : "input-field"}
          value={props.title}
          autoComplete={props.autoComplate}
          onChange={props.changed}
          placeholder={
            props.elementName === "Name"
              ? "Name"
              : props.elementName === "Age"
              ? "Age"
              : props.elementName === "Email"
              ? "Email"
              : props.elementName === "Password"
              ? "Password"
              : props.elementName === "Title"
              ? "Blog Title"
              : props.elementName === "Body"
              ? "Blog Content.."
              : "Text"
          }
        />
      );
  }

  return (
    <div>
      {props.elementName === "Name" ? (
        <label htmlFor="name">Your Name</label>
      ) : props.elementName === "Age" ? (
        <label htmlFor="age">Your Age</label>
      ) : props.elementName === "Email" ? (
        <label htmlFor="email">Your Email</label>
      ) : props.elementName === "Password" ? (
        <label htmlFor="password">Your Password</label>
      ) : props.elementName === "Title" ? (
        <label htmlFor="title">Titile:</label>
      ) : props.elementName === "Body" ? (
        <label htmlFor="body">Content:</label>
      ) : (
        <label htmlFor="name">Your Name</label>
      )}
      {inputElement}
      <div className="name error">
        {props.errorMessage !== null
          ? props.elementName === "Name"
            ? props.errorMessage.name
            : props.elementName === "Email"
            ? props.errorMessage.email
            : props.elementName === "Password"
            ? props.errorMessage.password
            : null
            ? props.changed
            : null
          : null}
      </div>
    </div>
  );
}

export default FormInput;
