import React from "react";
import "./BaseButton.css";

function BaseButton(props) {
  return (
    <div>
      <button disabled={props.disabled} className="btn-style" type="submit">
        {props.value}
      </button>
    </div>
  );
}

export default BaseButton;
