import React from "react";
import { connect } from "react-redux";

import "./Loader.css";

function Loader(props) {
  return props.loading ? <div className="loader" /> : null;
}

const mapStateToProps = (state) => {
  return {
    loading: state.userReducer.loading,
  };
};
export default connect(mapStateToProps)(Loader);
