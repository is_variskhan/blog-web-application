import React, { Component } from "react";
import { connect } from "react-redux";

import * as blogActions from "../../Redux/Actions/blogActionCreator";
import "./Blogs.css";
import BlogDetails from "../../components/BlogDetails/BlogDetails";

class Blogs extends Component {
  componentDidMount() {
    this.props.getBlogs();
  }

  selectPost = (id) => {
    this.props.history.push(`blogs/${id}`);
  };

  goToAddBlog = () => {
    this.props.history.push("/add-blog");
  };

  render() {
    let renderBlog;
    if (this.props.blogs.length) {
      renderBlog = this.props.blogs.map((item) => (
        <BlogDetails key={item._id} {...item} clicked={this.selectPost} />
      ));
    } else {
      renderBlog = (
        <div>
          <h2>No Any Blogs Posted Yet.</h2>
          <p>Please Add your First Blog Here..</p>
          <button onClick={this.goToAddBlog}>Add Blog</button>
        </div>
      );
    }
    return (
      <div className="blogs-container">
        {renderBlog}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    blogs: state.blogReducer.blogs,
    loading: state.blogReducer.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBlogs: () => dispatch(blogActions.getBlogs()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Blogs);
