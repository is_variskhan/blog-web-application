import React from "react";
import { connect } from "react-redux";
import { ReactComponent as EditLogo } from "../../assets/Icons/edit.svg";
import { ReactComponent as DeleteLogo } from "../../assets/Icons/delete.svg";
import { ReactComponent as CancelLogo } from "../../assets/Icons/close.svg";
import { ReactComponent as SaveLogo } from "../../assets/Icons/save.svg";

import "./SingleBlog.css";
import Loader from "../../common/Loader/Loader";
import FormInput from "../../common/FormInput/FormInput";
import * as blogActions from "../../Redux/Actions/blogActionCreator";

class SingleBlog extends React.Component {
  state = {
    formInputData: {
      title: {
        elementType: "input",
        elementFiledType: "text",
        elementName: "Title",
        value: "",
      },
      content: {
        elementType: "textarea",
        elementFiledType: "text",
        elementName: "Body",
        value: "",
      },
    },
    editable: false,
  };

  async componentDidMount() {
    this.fatchSingleBlog();
  }

  fatchSingleBlog() {
    let id = this.props.match.params.id;
    if (this.props.isAdmin) {
      this.props.getAllSingleBlog(id);
    } else {
      this.props.getSignleBlog(id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id !== this.props.match.params.id) {
      this.fatchSingleBlog();
    }
  }

  nextPostHandler = (id) => {
    this.props.history.push(`blogs/${id}`);
  };

  previousPostHandler = (id) => {
    this.props.history.push(`blogs/${id}`);
  };

  deleteHanlder = (id) => {
    this.props.deleteBlog(id);
    this.props.history.push("/blogs");
  };

  editBlogHandler = (params) => {
    this.setState((prevState) => ({
      ...prevState,
      editable: true,
      formInputData: {
        ...prevState.formInputData,
        title: {
          ...prevState.formInputData.title,
          value: this.props.blog.title,
        },
        content: {
          ...prevState.formInputData.content,
          value: this.props.blog.body,
        },
      },
    }));
  };

  cancelHanlder = () => {
    this.setState({ editable: false });
  };

  updateBlogHanlder = (id, params) => {
    const formData = {
      title: this.state.formInputData.title.value,
      body: this.state.formInputData.content.value,
    };
    this.props.updateBlog(id, formData);
    this.props.history.push("/blogs");
  };

  inputChangedHandler = (event, inputIndentifier) => {
    event.preventDefault();
    let clonFormData = {
      ...this.state.formInputData,
    };
    let innerCloneFormData = {
      ...clonFormData[inputIndentifier],
    };

    innerCloneFormData.value = event.target.value;

    clonFormData[inputIndentifier] = innerCloneFormData;

    this.setState({ formInputData: clonFormData });
  };

  goToPreviousPath = () => {
    this.props.history.goBack();
  };

  render() {
    const formElementArray = [];
    for (let key in this.state.formInputData) {
      formElementArray.push({
        id: key,
        config: this.state.formInputData[key],
      });
    }
    let isButtonShow;
    let post;
    if (this.props.match.params.id) {
      post = <Loader />;
    }
    if (!this.props.isAdmin) {
      isButtonShow = (
        <div>
          {!this.state.editable && (
            <div className="btn-position">
              <span className="edit-btn">
                <EditLogo onClick={() => this.editBlogHandler()} />
              </span>
              <span className="val-line" />
              <span className="delete-btn">
                <DeleteLogo
                  onClick={() => this.deleteHanlder(this.props.blog._id)}
                />
              </span>
            </div>
          )}
          {this.state.editable && (
            <div className="btn-position">
              <span className="edit-btn">
                <SaveLogo
                  className="save-logo"
                  onClick={() =>
                    this.updateBlogHanlder(this.props.blog._id, this.props.blog)
                  }
                />
              </span>
              <span className="val-line" />
              <span className="delete-btn">
                <CancelLogo onClick={() => this.cancelHanlder()} />
              </span>
            </div>
          )}
        </div>
      );
    }
    if (Object.keys(this.props.blog).length > 0) {
      post = (
        <div className="signle-post-container">
          <p className="previous" onClick={this.goToPreviousPath}>
            Go Back
          </p>
          {isButtonShow}
          <form className={!this.state.editable ? "signleBlog-form" : null}>
            {!this.state.editable && <h2> {this.props.blog.title}</h2>}
            {!this.state.editable && <h3> {this.props.blog.body}</h3>}
            {this.state.editable &&
              formElementArray.map((element) => (
                <FormInput
                  key={element.id}
                  autoComplate="off"
                  fieldName={element.config.elementFiledType}
                  elementName={element.config.elementName}
                  elementType={element.config.elementType}
                  value={element.config.value}
                  updateChanged={(event) =>
                    this.inputChangedHandler(event, element.id)
                  }
                  editable
                />
              ))}
          </form>
        </div>
      );
    } else {
      post = <h1 className="no-blog">Opps, We can't recognize the blog!</h1>;
    }
    return post;
  }
}

const mapStateToProps = (state) => {
  return {
    blog: state.blogReducer.signleBlog,
    isCreatedByUser: state.blogReducer.isCreatedByUser,
    loading: state.blogReducer.loading,
    isAdmin: state.userReducer.isRoleAdmin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllSingleBlog: (id) => dispatch(blogActions.getAllSingleBlog(id)),
    getSignleBlog: (id) => dispatch(blogActions.getSingleBlog(id)),
    deleteBlog: (id) => dispatch(blogActions.deleteBlog(id)),
    updateBlog: (id, parmas) => dispatch(blogActions.updateBlog(id, parmas)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleBlog);
