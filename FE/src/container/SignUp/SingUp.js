import React from "react";
import { connect } from "react-redux";

import "./SignUp.css";
import { Redirect } from "react-router-dom";
import FormInput from "../../common/FormInput/FormInput";
import BaseButton from "../../common/BaseButton/BaseButton";
import * as userActions from "../../Redux/Actions/userActionCreator";

class SignUp extends React.Component {
  state = {
    formInputData: {
      name: {
        elementType: "input",
        elementFiledType: "text",
        elementName: "Name",
        value: "",
      },
      age: {
        elementType: "input",
        elementFiledType: "number",
        elementName: "Age",
        value: "",
      },
      email: {
        elementType: "input",
        elementFiledType: "text",
        elementName: "Email",
        value: "",
      },
      password: {
        elementType: "input",
        elementFiledType: "password",
        elementName: "Password",
        value: "",
      },
    },
    submitData: {},
  };

  onSubmitHandler = (event) => {
    event.preventDefault();

    const formData = {
      name: this.state.formInputData.name.value,
      age: this.state.formInputData.age.value,
      email: this.state.formInputData.email.value,
      password: this.state.formInputData.password.value,
    };
    this.props.onSubmit(formData);

    this.setState({ name: "", age: "", email: "", password: "" });
  };

  inputChangedHandler = (event, inputIndentifier) => {
    event.preventDefault();
    this.props.isKeyPressToRemoveErrorMessage(event.target.value);
    let clonFormData = {
      ...this.state.formInputData,
    };
    let innerCloneFormData = {
      ...clonFormData[inputIndentifier],
    };

    innerCloneFormData.value = event.target.value;

    clonFormData[inputIndentifier] = innerCloneFormData;

    this.setState({ formInputData: clonFormData });
  };

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    const formElementArray = [];
    for (let key in this.state.formInputData) {
      formElementArray.push({
        id: key,
        config: this.state.formInputData[key],
      });
    }

    let form = (
      <form onSubmit={(event) => this.onSubmitHandler(event)}>
        <h2>Sign Up</h2>
        {formElementArray.map((element) => (
          <FormInput
            key={element.id}
            fieldName={element.config.elementFiledType}
            elementName={element.config.elementName}
            elementType={element.config.elementType}
            value={element.config.value}
            errorMessage={
              this.props.error && Object.keys(this.props.error).length > 0
                ? this.props.error
                : null
            }
            changed={(event) => this.inputChangedHandler(event, element.id)}
          />
        ))}
        <BaseButton value="Sing Up" />
      </form>
    );

    return <div className="container">{form}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    error: state.userReducer.error,
    user: state.userReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (payload) => dispatch(userActions.signUpApiCall(payload)),
    isKeyPressToRemoveErrorMessage: (payload) =>
      dispatch(userActions.isKeyPress(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
