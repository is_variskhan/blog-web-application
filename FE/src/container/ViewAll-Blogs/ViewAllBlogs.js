import React, { Component } from "react";
import { connect } from "react-redux";

import * as blogActions from "../../Redux/Actions/blogActionCreator";
import "./ViewAllBlogs.css";
import BlogDetails from "../../components/BlogDetails/BlogDetails";
import Loader from "../../common/Loader/Loader";

class ViewAllBlogs extends Component {
  componentDidMount() {
    this.props.getAllBlogs();
  }

  selectPost = (id) => {
    this.props.history.push(`blogs/${id}`);
  };

  render() {
    let renderBlog;
    if (this.props.blogs.length) {
      renderBlog = this.props.blogs.map((item) => (
        <BlogDetails key={item._id} {...item} clicked={this.selectPost} />
      ));
    } else {
      renderBlog = null;
    }
    return (
      <div className="blogs-container">
        {<Loader />}
        {renderBlog}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    blogs: state.blogReducer.blogs,
    loading: state.blogReducer.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllBlogs: () => dispatch(blogActions.getAllBlogs()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewAllBlogs);
