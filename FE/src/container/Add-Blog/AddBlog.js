import React from "react";
import { connect } from "react-redux";

import "./AddBlog.css";
import FormInput from "../../common/FormInput/FormInput";
import BaseButton from "../../common/BaseButton/BaseButton";
import * as blogAction from "../../Redux/Actions/blogActionCreator";

class AddBlog extends React.Component {
  state = {
    isFormValid: false,
    formInputData: {
      title: {
        elementType: "input",
        elementFiledType: "text",
        elementName: "Title",
        value: "",
        validation: {
          require: true,
          message: "Title is required.",
        },
        valid: false,
        touch: false,
      },
      body: {
        elementType: "textarea",
        elementFiledType: "text",
        elementName: "Body",
        value: "",
        validation: {
          require: true,
          message: "Blog is required.",
        },
        valid: false,
        touch: false,
      },
    },
    submitData: {},
  };

  onSubmitHandler = (event) => {
    event.preventDefault();

    const formData = {
      title: this.state.formInputData.title.value,
      body: this.state.formInputData.body.value,
    };
    this.props.addBlog(formData);

    this.setState({ title: "", body: "" });
    this.props.history.push("/blogs");
  };

  checkValidity(value, rules) {
    let isValid = true;

    if (rules.require) {
      isValid = value.trim() !== "" && isValid;
    }
    return isValid;
  }

  inputChangedHandler = (event, inputIndentifier) => {
    event.preventDefault();
    this.props.isKeyPressToRemoveErrorMessage(event.target.value);
    let clonFormData = {
      ...this.state.formInputData,
    };
    let innerCloneFormData = {
      ...clonFormData[inputIndentifier],
    };

    innerCloneFormData.value = event.target.value;

    innerCloneFormData.touch = true;

    clonFormData[inputIndentifier] = innerCloneFormData;

    innerCloneFormData.valid = this.checkValidity(
      innerCloneFormData.value,
      innerCloneFormData.validation,
      clonFormData
    );

    let fromDataCheck = true;
    for (let item in clonFormData) {
      fromDataCheck = clonFormData[item].valid && fromDataCheck;
    }

    this.setState({ formInputData: clonFormData, isFormValid: fromDataCheck });
  };

  render() {
    const formElementArray = [];
    for (let key in this.state.formInputData) {
      formElementArray.push({
        id: key,
        config: this.state.formInputData[key],
      });
    }

    let form = (
      <form onSubmit={(event) => this.onSubmitHandler(event)}>
        <h2>Add Blog</h2>
        {formElementArray.map((element) => (
          <FormInput
            key={element.id}
            fieldName={element.config.elementFiledType}
            elementName={element.config.elementName}
            elementType={element.config.elementType}
            value={element.config.value}
            isRequired=""
            autoComplate="off"
            errorMessage={
              this.props.error && Object.keys(this.props.error).length > 0
                ? this.props.error
                : null
            }
            changed={(event) => this.inputChangedHandler(event, element.id)}
          />
        ))}
        <BaseButton disabled={!this.state.isFormValid} value="Add Blog" />
      </form>
    );

    return <div className="container">{form}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.blogReducer.isAuthenticated,
    isDataFatchSuccessfully: state.blogReducer.isDataFatchSuccessfully,
    blogs: state.blogReducer.blogs,
    error: state.blogReducer.error,
    user: state.userReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addBlog: (payload) => dispatch(blogAction.addBlogApiCall(payload)),
    isKeyPressToRemoveErrorMessage: (payload) =>
      dispatch(blogAction.isKeyPress(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddBlog);
