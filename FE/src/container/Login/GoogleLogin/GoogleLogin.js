import React from "react";
import "./GoogleLogin.css";

function GoogleLogin(props) {
  return (
    <div className="container">
      <a
        href="http://localhost:3000/google-login"
        className="google google-btn"
      >
        <i className="fa fa-google fa-fw"></i> Sing in with Google
      </a>
    </div>
  );
}

export default GoogleLogin;
