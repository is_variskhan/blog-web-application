import React from "react";
import { connect } from "react-redux";

import FormInput from "../../common/FormInput/FormInput";
import BaseButton from "../../common/BaseButton/BaseButton";
import { Redirect } from "react-router-dom";
import * as userActions from "../../Redux/Actions/userActionCreator";
import "./Login.css";
import GoogleLogin from "./GoogleLogin/GoogleLogin";

class Login extends React.Component {
  state = {
    formInputData: {
      email: {
        elementType: "input",
        elementFiledType: "text",
        elementName: "Email",
        value: "",
      },
      password: {
        elementType: "input",
        elementFiledType: "password",
        elementName: "Password",
        value: "",
      },
    },
    redirectToReferrer: false,
    submitData: {},
  };

  onSubmitHandler = (event) => {
    event.preventDefault();
    const formData = {
      email: this.state.formInputData.email.value,
      password: this.state.formInputData.password.value,
    };
    this.props.onSubmit(formData);
    if (localStorage.getItem("token")) {
      this.setState(() => ({
        redirectToReferrer: true,
      }));
    }
    this.setState({ name: "", age: "", email: "", password: "" });
  };

  inputChangedHandler = (event, inputIndentifier) => {
    event.preventDefault();
    this.props.isKeyPressToRemoveErrorMessage(event.target.value);
    let clonFormData = {
      ...this.state.formInputData,
    };
    let innerCloneFormData = {
      ...clonFormData[inputIndentifier],
    };

    innerCloneFormData.value = event.target.value;

    clonFormData[inputIndentifier] = innerCloneFormData;

    this.setState({ formInputData: clonFormData });
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const { isAuthenticated } = this.props;

    if (isAuthenticated) {
      return <Redirect to={from} />;
    }

    const formElementArray = [];
    for (let key in this.state.formInputData) {
      formElementArray.push({
        id: key,
        config: this.state.formInputData[key],
      });
    }

    let form = (
      <form onSubmit={(event) => this.onSubmitHandler(event)}>
        <h2>Login</h2>
        {formElementArray.map((element) => (
          <FormInput
            key={element.id}
            fieldName={element.config.elementFiledType}
            elementName={element.config.elementName}
            elementType={element.config.elementType}
            value={element.config.value}
            errorMessage={
              this.props.error && Object.keys(this.props.error).length > 0
                ? this.props.error
                : {}
            }
            changed={(event) => this.inputChangedHandler(event, element.id)}
          />
        ))}
        <BaseButton value="Login" />
        <div className="google-login"></div>
        <hr />
        <div>
          <GoogleLogin />
        </div>
      </form>
    );

    return <div className="container">{form}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    redirectToReferrer: state.redirectToReferrer,
    error: state.userReducer.error,
    user: state.userReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (payload) => dispatch(userActions.userLogin(payload)),
    isKeyPressToRemoveErrorMessage: (payload) =>
      dispatch(userActions.isKeyPress(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
