import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./Home.css";
import * as userActions from "../../Redux/Actions/userActionCreator";

class Home extends Component {
  componentDidMount() {
    if (!this.props.user.name) {
      this.props.getUserProfile();
    }
  }

  render() {
    let userName;
    if (Object.keys(this.props.user).length === 0) {
      userName = <h2>Please login!</h2>;
    } else {
      userName = <h2>Welcome {this.props.user.name}</h2>;
    }
    return (
      <div className="headings">
        {userName}
        {!this.props.isAdmin ? (
          <Link to="/blogs" className="btn">
            View My Blogs
          </Link>
        ) : (
          <Link to="/viewAll-blogs" className="btn">
            View All Blogs
          </Link>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer.user,
    isAdmin: state.userReducer.isRoleAdmin,
    loading: state.loading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getUserProfile: () => dispatch(userActions.getLoggedInUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
