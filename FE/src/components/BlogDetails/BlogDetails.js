import React from "react";
import "./BlogDetails.css";
function BlogDetails(props) {
  return (
    <div
      className="blog-details-container"
      onClick={() => props.clicked(props._id)}
    >
      <h3 className="blog-title">{props.title.toUpperCase()}</h3>
      <p className="blog-content">{props.body}</p>
    </div>
  );
}

export default BlogDetails;
