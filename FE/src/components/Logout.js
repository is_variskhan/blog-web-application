import React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import * as userActions from "../Redux/Actions/userActionCreator";

function Logout(props) {
  return (
    <div>
      <NavLink to="/" onClick={() => props.onLogOut()}>
        logout
      </NavLink>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.userReducer.isLoggedIn,
    user: state.userReducer.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onLogOut: () => dispatch(userActions.logoutUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
