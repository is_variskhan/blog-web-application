import React, { useEffect } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Logout from "../Logout";
import "./NavBar.css";
import * as action from "../../Redux/Actions/userActionCreator";

function NavBar(props) {
  useEffect(() => {
    if (!props.user.email) {
      props.getUserProfile();
    }
  });

  let isUserLoggedIn;
  if (props.isAuthenticated) {
    isUserLoggedIn = (
      <ul>
        {!props.isAdmin ? (
          <NavLink className="add-blog" exact to="/add-blog">
            Add Blog
          </NavLink>
        ) : null}
        {props.isAdmin ? (
          <NavLink className="view-blog" exact to="/viewAll-blogs">
            View All Blogs
          </NavLink>
        ) : null}
        <li>Welcome, {props.user.name}</li>
        <li>
          <Logout />
        </li>
      </ul>
    );
  } else {
    isUserLoggedIn = (
      <ul>
        <li>
          <NavLink to="/login">Login</NavLink>
        </li>
        <li>
          <NavLink to="/signup">Sing Up</NavLink>
        </li>
      </ul>
    );
  }
  return (
    <div>
      <nav>
        <h1>
          {" "}
          <NavLink exact to="/">
            Home
          </NavLink>
        </h1>
        {isUserLoggedIn}
      </nav>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    user: state.userReducer.user,
    isAdmin: state.userReducer.isRoleAdmin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserProfile: () => dispatch(action.getLoggedInUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavBar));
