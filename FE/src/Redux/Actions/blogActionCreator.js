import * as actionType from "./actionType";
import axios from "axios";

export const loadingRequest = () => {
  return {
    type: actionType.LOADING_REQUEST,
  };
};

export const addBlog = (payload) => {
  return {
    type: actionType.ADD_BLOG,
    payload,
  };
};

export const fatchingBlog = () => {
  return {
    type: actionType.LOADING_REQUEST,
  };
};

export const fatchingBlogSuccess = (blog) => {
  return {
    type: actionType.BLOG_REQUEST_SUCCESS,
    payload: blog,
  };
};

export const fatchSignleBlog = (blog) => {
  return {
    type: actionType.FATCH_SINGLE_BLOG,
    payload: blog,
  };
};

export const fatchingFailuer = (error) => {
  return {
    type: actionType.BLOG_REQUEST_FAILUER,
    payload: error,
  };
};

export const isKeyPress = (payload) => {
  return {
    type: actionType.IS_KEY_PRESS,
    payload,
  };
};

export const addBlogApiCall = (params) => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      axios
        .post("/create-blog", params, config)
        .then((respone) => {
          const blog = respone.data;
          const getToken = localStorage.getItem("token");

          if (getToken) {
            dispatch(fatchingBlogSuccess(blog));
          }
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    const config = {
      headers: {
        Authorization: `Bearer ${googleToken}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      axios
        .post("/google-user/create-blog", params, config)
        .then((respone) => {
          const blog = respone.data;
          const getToken = localStorage.getItem("googleToken");

          if (getToken) {
            dispatch(fatchingBlogSuccess(blog));
          }
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  }
};

//get blogs by user id
export const getBlogs = () => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      if (token) {
        axios
          .get("/blogs?sortBy=createdAt:desc", config)
          .then((respone) => {
            const blog = respone.data;
            dispatch(fatchingBlogSuccess(blog));
          })
          .catch((error) => {
            dispatch(fatchingFailuer(error.response.data.errors));
          });
      }
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    const config = {
      headers: {
        Authorization: `Bearer ${googleToken}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      if (googleToken) {
        axios
          .get("/google-user/blogs?sortBy=createdAt:desc", config)
          .then((respone) => {
            const blog = respone.data;
            dispatch(fatchingBlogSuccess(blog));
          })
          .catch((error) => {
            dispatch(fatchingFailuer(error.response.data.errors));
          });
      }
    };
  }
};

//get admin all blogs
export const getAllBlogs = () => {
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return function (dispatch) {
    dispatch(loadingRequest());
    if (token) {
      axios
        .get("/all-blogs", config)
        .then((respone) => {
          const blogs = respone.data;
          dispatch(fatchingBlogSuccess(blogs));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    }
  };
};

export const getSingleBlog = (id) => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      axios
        .get(`/blogs/${id}`, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchSignleBlog(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    const config = {
      headers: {
        Authorization: `Bearer ${googleToken}`,
      },
    };
    return function (dispatch) {
      dispatch(loadingRequest());
      axios
        .get(`/google-user/blogs/${id}`, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchSignleBlog(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  }
};

export const getAllSingleBlog = (id) => {
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return function (dispatch) {
    dispatch(loadingRequest());
    axios
      .get(`/all-blogs/${id}`, config)
      .then((respone) => {
        const blog = respone.data;
        dispatch(fatchSignleBlog(blog));
      })
      .catch((error) => {
        dispatch(fatchingFailuer(error.response.data.errors));
      });
  };
};

export const deleteBlog = (id) => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    return function (dispatch) {
      axios
        .delete(`/blogs/${id}`, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchingBlogSuccess(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    const config = {
      headers: {
        Authorization: `Bearer ${googleToken}`,
      },
    };
    return function (dispatch) {
      axios
        .delete(`/google-user/blogs/${id}`, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchingBlogSuccess(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  }
};

export const updateBlog = (id, params) => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    return function (dispatch) {
      axios
        .patch(`/blogs/${id}`, params, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchingBlogSuccess(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    const config = {
      headers: {
        Authorization: `Bearer ${googleToken}`,
      },
    };
    return function (dispatch) {
      axios
        .patch(`/google-user/blogs/${id}`, params, config)
        .then((respone) => {
          const blog = respone.data;
          dispatch(fatchingBlogSuccess(blog));
        })
        .catch((error) => {
          dispatch(fatchingFailuer(error.response.data.errors));
        });
    };
  }
};
