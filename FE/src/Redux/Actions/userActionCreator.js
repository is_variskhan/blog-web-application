import * as actionType from "./actionType";
import axios from "axios";

export const signUpUser = (payload) => {
  return {
    type: actionType.SING_UP_USER,
    payload,
  };
};

export const loadingRequest = () => {
  return {
    type: actionType.LOADING_REQUEST,
  };
};

export const signUpSuccess = (user) => {
  return {
    type: actionType.SING_UP_SUCCESS,
    payload: user,
  };
};

export const singUpFailuer = (error) => {
  return {
    type: actionType.SING_UP_FAILUER,
    payload: error,
  };
};

export const getuser = () => {
  return {
    type: actionType.GET_USER_PROFILE,
  };
};

export const logout = () => {
  return {
    type: actionType.LOGOUT_USER,
  };
};

export const isKeyPress = (payload) => {
  return {
    type: actionType.IS_KEY_PRESS,
    payload,
  };
};

export const signUpApiCall = (params) => {
  return function (dispatch) {
    dispatch(loadingRequest());
    axios
      .post("/singup", params)
      .then((respone) => {
        const user = respone.data;
        localStorage.setItem("token", user.token);
        const getToken = localStorage.getItem("token");

        if (getToken) {
          dispatch(signUpSuccess(user));
        }
      })
      .catch((error) => {
        dispatch(singUpFailuer(error.response.data.errors));
      });
  };
};

export const getGoogleUserProfile = () => {
  const token = localStorage.getItem("google-token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return function (dispatch) {
    dispatch(loadingRequest());
    axios
      .get("/google-user/me", config)
      .then((respone) => {
        const user = respone.data;
        const getToken = localStorage.getItem("google-token");
        if (getToken) {
          dispatch(signUpSuccess(user));
        }
      })
      .catch((error) => {
        dispatch(singUpFailuer(error.response.data.errors));
      });
  };
};

export const getLoggedInUser = () => {
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  return function (dispatch) {
    dispatch(loadingRequest());
    if (token) {
      axios
        .get("/users/me", config)
        .then((respone) => {
          const user = respone.data;
          dispatch(signUpSuccess(user));
        })
        .catch((error) => {
          dispatch(singUpFailuer(error.response.data.errors));
        });
    }
  };
};

export const userLogin = (params) => {
  return function (dispatch) {
    dispatch(loadingRequest());
    axios
      .post("/login", params)
      .then((respone) => {
        const user = respone.data;
        localStorage.setItem("token", user.token);
        const getToken = localStorage.getItem("token");
        if (getToken) {
          dispatch(signUpSuccess(user));
        }
      })
      .catch((error) => {
        dispatch(singUpFailuer(error.response.data.errors));
      });
  };
};

export const logoutUser = () => {
  const token = localStorage.getItem("token");
  if (token !== null) {
    return function (dispatch) {
      dispatch(loadingRequest());
      if (token) {
        axios
          .post("/users/logout")
          .then((respone) => {
            localStorage.removeItem("token");
            dispatch(logout());
            window.location.replace("/");
          })
          .catch((error) => {
            dispatch(singUpFailuer(error.response.data.errors));
          });
      }
    };
  } else {
    const googleToken = localStorage.getItem("google-token");
    return function (dispatch) {
      dispatch(loadingRequest());
      if (googleToken) {
        axios
          .post("/google-user/logout")
          .then((respone) => {
            localStorage.removeItem("google-token");
            dispatch(logout());
            window.location.replace("/");
          })
          .catch((error) => {
            dispatch(singUpFailuer(error.response.data.errors));
          });
      }
    };
  }
};
