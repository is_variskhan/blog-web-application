import * as actionType from "../Actions/actionType";

const initialState = {
  blogs: [],
  signleBlog: {},
  loading: false,
  token: localStorage.getItem("token"),
  isAuthenticated: false,
  error: {},
};

const blogReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_BLOG: {
      return {
        ...state,
        blogs: action.payload,
      };
    }
    case actionType.LOADING_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionType.BLOG_REQUEST_SUCCESS:
      return {
        ...state,
        loading: false,
        blogs: action.payload,
        isAuthenticated: true,
        error: {},
      };
    case actionType.BLOG_REQUEST_FAILUER:
      return {
        ...state,
        loading: false,
        blogs: [],
        isAuthenticated: false,
        error: action.payload,
      };
    case actionType.FATCH_SINGLE_BLOG:
      return {
        ...state,
        loading: false,
        signleBlog: action.payload,
        error: "",
      };
    case actionType.IS_KEY_PRESS:
      return {
        ...state,
        error: {},
      };
    default:
      return state;
  }
};

export default blogReducer;
