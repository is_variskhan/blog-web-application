import * as actionType from "../Actions/actionType";

const initialState = {
  user: {},
  loading: false,
  token: localStorage.getItem("token"),
  isAuthenticated: false,
  isRoleAdmin: false,
  error: {},
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SING_UP_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }
    case actionType.LOADING_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionType.SING_UP_SUCCESS:
      let isRole = false;
      if (action.payload.role === "admin") {
        isRole = true;
      }
      return {
        ...state,
        loading: false,
        user: action.payload,
        isRoleAdmin: isRole,
        isAuthenticated: true,
        error: {},
      };
    case actionType.SING_UP_FAILUER:
      localStorage.removeItem("token");
      return {
        ...state,
        loading: false,
        user: {},
        isRoleAdmin: false,
        isAuthenticated: false,
        error: action.payload,
      };
    case actionType.GET_USER_PROFILE:
      return {
        ...state,
        loading: false,
        isAuthenticated: true,
        user: action.payload,
        error: {},
      };
    case actionType.LOGOUT_USER:
      localStorage.removeItem("token");
      localStorage.removeItem("google-token");
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        isRoleAdmin: false,
        user: {},
        error: {},
      };
    case actionType.IS_KEY_PRESS:
      return {
        ...state,
        error: {},
      };
    default:
      return state;
  }
};

export default userReducer;
