import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter,
} from "react-router-dom";
import queryString from "query-string";
import NavBar from "./components/NavBar/NavBar";
import Home from "./container//Home/Home";
import Footer from "./components/Footer";
import Login from "./container/Login/Login";
import SignUp from "./container/SignUp/SingUp";
import Blogs from "./container/Blogs/Blogs";
import PrivateRoute from "./container/PrivateRoute";
import { connect } from "react-redux";
import AddBlog from "./container/Add-Blog/AddBlog";
import SingleBlog from "./container/SingleBlog/SingleBlog";
import ViewAllBlogs from "./container/ViewAll-Blogs/ViewAllBlogs";
import * as userActions from "./Redux/Actions/userActionCreator";
class App extends Component {
  state = {
    isGoogleUser: false,
  };

  componentDidMount() {
    let query = queryString.parse(this.props.location.search);
    if (query.token) {
      localStorage.setItem("google-token", query.token);
      this.props.history.push("/");
    }
    if (localStorage.getItem("google-token")) {
      this.props.getGoogleUserProfile();
    }
  }

  render() {
    return (
      <div className="App" style={{ minHeight: "100vh" }}>
        <NavBar />
        <Switch>
          <Route exact path="/" component={Home} />
          {!this.props.isAdmin ? (
            <PrivateRoute
              exact
              path="/add-blog"
              authenticatedUser={this.props.isAuthenticated}
              component={AddBlog}
            />
          ) : null}
          <Route exact path="/signup" component={SignUp} />
          <Route path="/login" component={Login} />
          {this.props.isAdmin ? (
            <Route path="/viewAll-blogs" component={ViewAllBlogs} />
          ) : null}
          <PrivateRoute
            path="/blogs"
            exact
            authenticatedUser={this.props.isAuthenticated}
            component={Blogs}
          />
          <PrivateRoute
            path="/blogs/:id"
            exact
            authenticatedUser={this.props.isAuthenticated}
            component={SingleBlog}
          />
        </Switch>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userReducer.isAuthenticated,
    user: state.userReducer.user,
    isAdmin: state.userReducer.isRoleAdmin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGoogleUserProfile: () => dispatch(userActions.getGoogleUserProfile()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
